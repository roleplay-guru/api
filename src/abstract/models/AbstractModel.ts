import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import { CreateDateColumn, DeleteDateColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@ObjectType({ isAbstract: true })
export abstract class AbstractModel {
    @PrimaryGeneratedColumn('uuid')
    @Field({ nullable: false })
    id: string;

    @CreateDateColumn()
    @Field(() => GraphQLISODateTime)
    createdAt: Date;

    @UpdateDateColumn()
    @Field(() => GraphQLISODateTime)
    updatedAt: Date;

    @DeleteDateColumn()
    @Field(() => GraphQLISODateTime, { nullable: true, defaultValue: null })
    deletedAt: Date;
}
