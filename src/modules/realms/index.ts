import { Module } from '@nestjs/common';
import { RealmResolver } from './services/RealmResolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Realm } from './models/Realm';
import { RealmService } from './services/RealmService';

@Module({
    imports: [TypeOrmModule.forFeature([Realm])],
    providers: [RealmResolver, RealmService],
    exports: [TypeOrmModule],
})
export class RealmsModule {}
