import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { Realm } from '../models/Realm';
import { CreateRealmInput } from '../dto/CreateRealmInput';
import { RealmService } from './RealmService';
import { SessionGuard } from '../../auth/services/SessionGuard';
import { UseGuards } from '@nestjs/common';

@Resolver(() => Realm)
export class RealmResolver {
    constructor(private readonly realmService: RealmService) {}

    @Mutation(() => Realm)
    createRealm(@Args('createRealmInput') createRealmInput: CreateRealmInput) {
        return this.realmService.create(createRealmInput);
    }

    @UseGuards(SessionGuard)
    @Query(() => Realm)
    getRealm(@Args('realmId') realmId: string) {
        return this.realmService.findOne(realmId);
    }
}
