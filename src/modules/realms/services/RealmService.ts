import { Repository } from 'typeorm';
import { Realm } from '../models/Realm';
import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateRealmInput } from '../dto/CreateRealmInput';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class RealmService {
    constructor(
        @InjectRepository(Realm)
        private readonly realmRepository: Repository<Realm>,
    ) {}

    async create(createRealmInput: CreateRealmInput) {
        const realm = this.realmRepository.create(createRealmInput);
        return this.realmRepository.save(realm);
    }

    async findOne(id: string): Promise<Realm> {
        const realm = await this.realmRepository.findOne(id);

        if (!realm) {
            throw new NotFoundException(`Realm '${id}' not found.`);
        }

        return realm;
    }
}
