import { Field, ObjectType } from '@nestjs/graphql';
import { AbstractModel } from '../../../abstract/models/AbstractModel';
import { Column, Entity } from 'typeorm';

@Entity('realms')
@ObjectType({ description: 'The resource used to describe a RPG Realm.' })
export class Realm extends AbstractModel {
    @Column({ type: 'varchar', length: 100 })
    @Field({ nullable: false })
    title: string;

    @Column({ type: 'varchar', length: 1000 })
    @Field({ nullable: false })
    description: string;

    @Column({ type: 'char', length: 2 })
    @Field({ nullable: false, description: 'Language spoken in this realm' })
    language: string;

    @Column({ type: 'boolean' })
    @Field(() => Boolean, { description: 'Are underage users restricted?' })
    isRestricted: boolean;
}
