import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateRealmInput {
    @Field({ nullable: false })
    title: string;

    @Field({ nullable: false })
    description: string;

    @Field({ nullable: false, description: 'Language spoken in this realm' })
    language: string;

    @Field(() => Boolean, { description: 'Are underage users restricted?' })
    isRestricted: boolean;
}
