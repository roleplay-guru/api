import { Account } from '../../accounts/models/Account';
import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn,
    JoinColumn,
    ManyToOne,
} from 'typeorm';

@Entity('sessions')
export class Session {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @ManyToOne(() => Account)
    @JoinColumn()
    account: Account;

    @Column({ type: 'char', length: 128 })
    hash: string;

    @Column({ type: 'varchar', length: 45 })
    ipAddress: string;

    @Column({ type: 'datetime' })
    expiresAt: Date;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}
