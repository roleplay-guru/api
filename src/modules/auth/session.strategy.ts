import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-cookie';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './services/AuthService';

@Injectable()
export class SessionStrategy extends PassportStrategy(Strategy, 'session') {
    constructor(private readonly authService: AuthService) {
        super({ cookieName: 'RPG_TOKEN' });
    }

    async validate(token) {
        const session = await this.authService.checkUserSession(token);

        if (!session) {
            throw new UnauthorizedException('ERR_UNAUTHORIZED');
        } else if (session.expiresAt < new Date()) {
            throw new UnauthorizedException('ERR_SESSION_EXPIRED');
        }

        return session.account;
    }
}
