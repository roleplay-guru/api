import { Module } from '@nestjs/common';
import { AccountsModule } from '../accounts';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './services/AuthService';
import { SessionStrategy } from './session.strategy';
import { AuthResolver } from './services/AuthResolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Session } from './models/Session';
import { APP_GUARD } from '@nestjs/core';
import { SessionGuard } from './services/SessionGuard';

@Module({
    imports: [AccountsModule, PassportModule, TypeOrmModule.forFeature([Session])],
    providers: [
        AuthService,
        AuthResolver,
        SessionStrategy,
        {
            provide: APP_GUARD,
            useClass: SessionGuard,
        },
    ],
})
export class AuthModule {}
