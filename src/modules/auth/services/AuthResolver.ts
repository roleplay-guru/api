import { Args, Context, Mutation, Resolver } from '@nestjs/graphql';
import { Public } from '../decorators/Public';
import { AuthenticationOutput } from '../dto/AuthenticationOutput';
import { AuthService } from './AuthService';
import { Ip } from '../../common/decorators/Ip';

@Resolver()
export class AuthResolver {
    constructor(private readonly authService: AuthService) {}

    @Public()
    @Mutation(() => AuthenticationOutput)
    async authenticate(
        @Args('email') email: string,
        @Args('passwd') passwd: string,
        @Args('rememberMe') rememberMe: boolean,
        @Ip() ipAddress: string,
        @Context('req') request,
    ) {
        const account = await this.authService.checkCredentials(email, passwd);

        const session = await this.authService.login(account, ipAddress);

        request.res?.cookie('RPG_TOKEN', session.hash);

        return { success: true };
    }
}
