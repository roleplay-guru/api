import { Injectable } from '@nestjs/common';
import { AccountService } from '../../accounts/services/AccountService';
import { Account } from '../../accounts/models/Account';
import { HashingService } from '../../common/services/HashingService';
import { Repository } from 'typeorm';
import { Session } from '../models/Session';
import { InjectRepository } from '@nestjs/typeorm';
import { addHours } from 'date-fns';
import { InvalidCredentials } from '../utils/exceptions/InvalidCredentials';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(Session)
        private readonly sessionRepository: Repository<Session>,
        private readonly accountService: AccountService,
        private readonly hashingService: HashingService,
    ) {}

    async checkCredentials(email: string, password: string): Promise<Account | null> {
        const account = await this.accountService.findByEmail(email);

        if (!account) {
            throw new InvalidCredentials(account);
        } else if (account.isDisabled) {
            throw new InvalidCredentials(account, 'ERR_ACCOUNT_DISABLED');
        }

        const isCorrectPassword = await this.hashingService.compareBcrypt(password, account.passwd);

        if (!isCorrectPassword) {
            throw new InvalidCredentials(account);
        }

        return account;
    }

    async login(account: Account, ipAddress: string): Promise<Session> {
        const token = this.hashingService.hash(account.id + Date.now());

        const session = this.sessionRepository.create({
            account,
            hash: token,
            ipAddress,
            expiresAt: addHours(new Date(), 1),
        });

        return this.sessionRepository.save(session);
    }

    async checkUserSession(token: string): Promise<Session> {
        return this.sessionRepository.findOne({
            where: { hash: token },
            relations: ['account'],
        });
    }
}
