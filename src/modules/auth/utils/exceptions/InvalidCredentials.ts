import { UnauthorizedException } from '@nestjs/common';
import { Account } from '../../../accounts/models/Account';

export class InvalidCredentials extends UnauthorizedException {
    constructor(private readonly account?: Account, message?: string) {
        super(message || 'ERR_INVALID_CREDENTIALS');
    }

    getAccount(): Account {
        return this.account;
    }
}
