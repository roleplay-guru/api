import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const Ip = createParamDecorator((data: unknown, context: ExecutionContext): string => {
    const ctx = GqlExecutionContext.create(context);

    return ctx.getContext().req.ip;
});
