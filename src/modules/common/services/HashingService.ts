import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';
import * as crypto from 'crypto';

@Injectable()
export class HashingService {
    hash(token: string): string {
        return crypto.createHash('sha512').update(token).digest('hex');
    }

    async createBcryptHash(token: string): Promise<string> {
        return bcrypt.hash(token, 6);
    }

    async compareBcrypt(plainString: string, hash: string): Promise<boolean> {
        return bcrypt.compare(plainString, hash);
    }
}
