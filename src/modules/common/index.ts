import { Global, Module } from '@nestjs/common';
import { HashingService } from './services/HashingService';

@Global()
@Module({
    providers: [HashingService],
    exports: [HashingService],
})
export class CommonModule {}
