import { BadRequestException, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Account } from '../models/Account';
import { InjectRepository } from '@nestjs/typeorm';
import { RegisterAccountInput } from '../dto/RegisterAccountInput';
import { HashingService } from '../../common/services/HashingService';

@Injectable()
export class AccountService {
    constructor(
        @InjectRepository(Account)
        private readonly accountsRepository: Repository<Account>,
        private readonly hashingService: HashingService,
    ) {}

    async createAccount(accountDetails: RegisterAccountInput) {
        const hash = await this.hashingService.createBcryptHash(accountDetails.passwd);

        const existingAccount = await this.findByEmail(accountDetails.email);

        if (existingAccount) {
            throw new BadRequestException('ERR_EXISTING_EMAIL');
        }

        const account = this.accountsRepository.create({
            email: accountDetails.email,
            passwd: hash,
            isDisabled: false,
        });

        return this.accountsRepository.save(account);
    }

    async findByEmail(email: string): Promise<Account> {
        return this.accountsRepository.findOne({ email });
    }
}
