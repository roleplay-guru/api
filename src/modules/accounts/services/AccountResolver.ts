import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { Account } from '../models/Account';
import { RegisterAccountInput } from '../dto/RegisterAccountInput';
import { AccountService } from './AccountService';

@Resolver(() => Account)
export class AccountResolver {
    constructor(private readonly accountService: AccountService) {}

    @Mutation(() => Account, { name: 'register_account' })
    async registerAccount(@Args('details') accountDetails: RegisterAccountInput) {
        return this.accountService.createAccount(accountDetails);
    }
}
