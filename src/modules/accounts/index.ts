import { Module } from '@nestjs/common';
import { AccountService } from './services/AccountService';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './models/Account';
import { AccountResolver } from './services/AccountResolver';

@Module({
    imports: [TypeOrmModule.forFeature([Account])],
    providers: [AccountService, AccountResolver],
    exports: [TypeOrmModule, AccountService],
})
export class AccountsModule {}
