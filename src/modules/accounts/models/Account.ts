import { AbstractModel } from '../../../abstract/models/AbstractModel';
import { Column, Entity } from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

@Entity('accounts')
@ObjectType({ description: "The resource used to describe player's accounts." })
export class Account extends AbstractModel {
    @Column({ type: 'nvarchar', length: 128 })
    @Field({ nullable: false })
    email: string;

    @Column({ type: 'varchar', length: 60 })
    passwd: string;

    @Column({ type: 'datetime', default: null })
    @Field({ nullable: true, defaultValue: null })
    lastLoginAt: Date;

    @Column({ type: 'boolean', default: false })
    @Field({ nullable: false, defaultValue: false })
    isDisabled: boolean;
}
