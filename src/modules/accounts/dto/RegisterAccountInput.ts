import { Field, GraphQLISODateTime, InputType } from '@nestjs/graphql';

@InputType()
export class RegisterAccountInput {
    @Field({ nullable: false })
    email: string;

    @Field({ nullable: false })
    passwd: string;

    @Field(() => GraphQLISODateTime, { nullable: false })
    birthDate: Date;
}
