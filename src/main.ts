import { NestFactory } from '@nestjs/core';
import { RootModule } from './modules';
import * as cookieParser from 'cookie-parser';

async function bootstrap() {
    const app = await NestFactory.create(RootModule);
    app.use(cookieParser());
    await app.listen(8000);
}

bootstrap();
